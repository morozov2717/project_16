const mathOperations = {
    addition: function(...arg){
        sum = 0
        if(arguments.length == 1){
            return arguments[0]
        }
        else{
            for(i=0;i<arguments.length;i++)
            sum += arguments[i]
        }
        return sum
    },
    subtraction: function(...arg){
        sum = arguments[0]
        if(arguments.length == 1){
            return arguments[0]
        }
        else{
            for(i=1;i<arguments.length;i++)
            sum -= arguments[i]
        }
        return sum
    },
    multiplication: function(...arg){
        sum = arguments[0]
        if(arguments.length == 1){
            return arguments[0]
        }
        else{
            for(i=1;i<arguments.length;i++)
            sum *= arguments[i]
        }
        return sum
    },
    division: function(...arg){
        sum = arguments[0]
        if(arguments.length == 1){
            return arguments[0]
        }
        else{
            for(i=1;i<arguments.length;i++)
            sum /= arguments[i]
        }
        return sum
    },
}
module.exports = mathOperations